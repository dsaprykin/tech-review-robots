<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MIT TechReview</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="assets/css/screen.css">

        <script src="assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <header>
            <div id="top"></div>
            <div id="backgrounds" data-role="backgrounds">
                <div id="desert"></div>
                <div id="jumper"></div>
                <div id="beach"></div>
                <div id="petman"></div>
            </div>
        </header>

        <article>
            <section id="title">
                <div class="wrap">
                <!-- H1: fades in on doc.ready -->
                <h1 style="display:none;">The robots<br /> running<br />this way</h1>
                <p class="lead">Boston Dynamics is building robots that walk and run like living creatures. Some of these machines are now headed for the world’s toughest terrain.</p>
                <p class="by-line">By Will Knight</p>
            </div>
            </section>
            <!-- INTRO -->
            <section id="intro">
                <div class="wrap">
                    <p class="drop-caps">A</p>
                    <p>In the pit lane of the Homestead-Miami speedway in Florida, inside a track on which race cars sometimes travel at over 300 kilometers an hour, a small crowd is watching something considerably slower but arguably far more impressive. On a sunny Saturday morning just before Christmas, a robot that roughly resembles a large person is contemplating a makeshift door on the tarmac ahead. It surveys the door using a laser scanner and a pair of cameras in its head; then, after a lengthy pause, the robot extends a gleaming aluminum arm, pushes open the door, and slowly steps through.</p>

                    <p>The robot, called Atlas and made by Boston Dynamics, is competing in <a href="http://theroboticschallenge.org/" target="_blank">the DARPA Robotics Contest</a>, organized by the U.S. Defense Advanced Projects Research Agency. Over the weekend, robots of varying shape and design, all controlled remotely, attempt challenges meant to test the limits of artificial sensing, manipulation, and agility. Each task is inspired by work that could help stem a leak at a stricken nuclear power plant. The jobs are seemingly simple, but not for robots. In one, the machines must get across a pile of rubble without toppling over; in another they have to climb a tall ladder.</p>

                    <p>Many of the robots struggle to complete the tasks without malfunctioning, freezing up, or toppling over. Of all the challenges facing these robots, one of the most difficult, and potentially the most important to master, is simply walking over uneven, unsteady, or just cluttered ground. But the Atlas robots (several academic groups have entered versions of the Boston Dynamics machine) walk across such terrain with impressive confidence.</p>

                    <p>A couple of times each day, the crowd gets to see two other legged robots made by <a href="http://bostondynamics.com/" target="_blank">Boston Dynamics</a>). In one demo, a four-legged machine about the size of a horse trots along the track carrying several large packs; it cleverly shuffles its feet to stay upright when momentarily unbalanced by a hefty kick from its operator. In another, a smaller, more agile four-legged machine revs up a loud diesel engine, then bounds maniacally along the racetrack like a big cat, quickly reaching almost 20 miles per hour.</p>

                    <p>The crowd, filled with robotics researchers from around the world and curious members of the public, gasps and applauds. But the walking and running technology found in the machines developed by Boston Dynamics are more than just dazzling. If the technology can be improved, then these robots, and others like them, might stride out of research laboratories and populate the world with smart mobile machines. That helps explain why a few days before the DARPA Challenge, Boston Dynamics was acquired by Google.</p>
                </div>
            </section>

            <!-- <div id="partOneBumper" data-role="background">
                <img src="assets/imgs/part1-background.jpg" alt="Part one bumper background">
            </div> -->

            <!-- SECTION 1: Learning to Hop -->
            <section id="partOne" class="clearfix">
                <div class="title text-center">
                    <p>Part 1</p>
                    <h1>Learning to Hop</h1>
                </div>
                <div class="bg">
                    <div class="wrap">

                        <div class="text right">
                            <p><strong>Part 1.</strong> A few months before the DARPA contest, I visited Boston Dynamics, which occupies an ordinary-looking building on the edge of a quiet industrial park in Waltham, Massachusetts, a 20-minute drive from Boston. In the entrance, four-legged robots of various shape and size appear to stand guard. Within the large workshop inside, dozens of engineers tinkered away on all manner of mechanical beast. In one corner, a small four-legged machine with a long neck and a gripper instead of a head was using the appendage to hurl cinder blocks across the floor.</p>
                            
                            <p>All of these machines have their origins in groundbreaking work done by Marc Raibert, the founder and chief technology officer of Boston Dynamics. On the wall of Raibert’s office, next to a large poster showing Atlas in great technical detail, is a small poster identifying various dinosaurs. He recalls becoming interested in animal locomotion while studying for a PhD in the brain and cognitive sciences department at MIT in the late 1970s, when two prominent physiologists came to talk about research on cat locomotion. Fascinated by how a brain can produce such effortless agility, Raibert hatched a plan to start building machines to explore the phenomenon when he landed a job as an assistant professor at Carnegie Mellon University in 1980.</p>
                            
                            <p>Other academics had built walking machines. Some had many legs, to ensure that lifting one of them to walk forward would not unbalance them. Others moved extremely carefully and deliberately to maintain a precarious balance. The machines were clumsy, slow, and altogether a poor imitation of most biological locomotion. In many cases, even the slightest slip or push would cause those machines to topple over.</p>
                            
                            <p>Demonstrating a remarkable insight, Raibert decided his first walking robot would not be designed to avoid the instability that motion can introduce, it would embrace it. Instead of six legs or even four he gave it just one.</p>
                            
                            <p>The robot would have to bounce on its single leg, assessing its own movement and orientation with each leap, and quickly adjust the position of its leg and body as well as how much energy its leg would expend with the next hop. The calculations were surprisingly simple.</p>

                            <!-- <div class="video">
                                <img src="assets/imgs/600.gif" alt="">
                            </div> -->

                            <p><strong>Part 2.</strong> Remarkably, the robot worked perfectly, hopping around like a possessed pogo stick. While the first version was limited in its movement, the next could hop freely around the lab. “I can still remember—I think it was a day in August in 1983,” Raibert recalls. “We were all just standing around grinning. We would push the machine and it would travel across the room until the other guy got it and then he would push it back.”</p>
                            
                            <p>Raibert knew that a leaping animal becomes unbalanced as it jumps and must constantly adjust, and that it uses gravity to move itself along. The rudimentary hopping robot solved the same problems, and showed how to build more nimble machines. “It looked to me like the dynamics of [biological movement], where there's a lot of energy and motion, where there's tipping all the time, that those were really the characteristics you wanted to get,” he recalls.</p>
                            
                            <p>Inspired by the success of the approach, Raibert and his students started building other legged machines using what roboticists call dynamic balance—an ability to use movement to maintain balance. The next version had four legs and trotted along by bouncing alternately on front and back legs on opposite sides. Other robots had far more sophisticated joints, actuators, and control software.</p>

                            <p>Raibert’s “Leg Lab” moved from CMU to MIT in 1986, where it developed other robots that could walk, bounce, run, and jump in ways that often seemed oddly recognizable. Machines had names inspired by biological counterparts. <a href="http://www.ai.mit.edu/projects/leglab/old-leglab/robots/Spring_Flamingo/Spring_Flamingo.html" target="_blank">Spring Flamingo</a> and <a href="http://www.ai.mit.edu/projects/leglab/old-leglab/robots/Spring_Turkey/Spring_Turkey.html" target="_blank">Spring Turkey</a> would strut around the lab like giant birds, while <a href="http://www.ai.mit.edu/projects/leglab/old-leglab/robots/uniroo/uniroo.html" target="_blank">Uniroo</a> hopped along using a tail for balance, like an awkward, one-legged kangaroo.</p>

                            <p>Raibert founded Boston Dynamics in 1995, initially to sell simulation software developed at his lab. But the company also consulted on commercial robotics projects, including the development of <a href="http://www.sony-europe.com/support/aibo/4_0_support_breakdown.asp" target="_blank">AIBO</a> and <a href="http://www.sony.net/SonyInfo/CorporateInfo/History/sonyhistory-j.html" target="_blank">QRIO</a>, robotic toys by Sony in 1999 and 2003 respectively. And contract with DARPA, in 2003, saw Boston Dynamics start making its own legged machines.</p>
                        </div> <!-- /end .text -->

                        <div class="robots left hide-for-mobile">
                            <div id="hopper" class="robot">
                                <div class="affix-wrap">
                                    <img src="assets/imgs/raibert-hopper.jpg" alt="hopper robot">
                                </div>
                                <small>The 3-D version of Raibert’s one-legged dynamic balancing robot was capable of bouncing around his lab.</small>
                            </div>

                            <div id="biped" class="robot">
                                <div class="affix-wrap">
                                    <img src="assets/imgs/3dbiped.jpg" alt="biped robot">
                                </div>
                                <small>Robots developed later had more than one leg. This 3-D biped was able to perform somersaults.</small>
                            </div>
                        </div> <!-- /end .robots -->

                    </div> <!-- /end .wrap -->
                </div> <!-- /end .bg -->

                <!-- flip robot flip book -->
                <div id="hopper-flip-book">
                    <div class="page-1"></div>
                    <div class="page-2"></div>
                    <div class="page-3"></div>
                    <div class="page-4"></div>
                    <div class="page-5"></div>
                </div>
                <!-- /flip robot flip book -->
               
            </section>

            <!-- <div id="partTwoBumper" class="background-video" data-role="background"></div> -->
            <!-- SECTION 2: Learning to Crawl -->

            <section id="partTwo" class="clearfix">
                <div class="title text-center">
                    <p>Part 2</p>
                    <h1>Learning to Run</h1>
                </div>
                <div class="bg">
                    <div class="wrap">
                        <img src="assets/imgs/bigdog.jpg" alt="">

                        <div class="inner-wrap">

                            <div class="video right hide-for-mobile">
                                <div class="flex-video">
                                    <object id="myExperience3152796575001" class="BrightcoveExperience"><!-- big dog woods -->
                                        <param name="playerID" value="3164827754001" />
                                        <param name="playerKey" value="AQ~~,AAAAAAEgZvo~,jStb8wH-jnK4A8jPSw5ObvdyN2i4vafT" />
                                        <param name="isVid" value="true" />
                                        <param name="isUI" value="true" />
                                        <param name="dynamicStreaming" value="true" />
                                        <param name="@videoPlayer" value="3162154088001" />
                                    </object>
                                </div>
                            </div>

                            <div class="text left">
                                <h2>Big Dog</h2>

                                <p>In 2003, armed with a DARPA contract for creating a prototype vehicle capable of following troops across ground inaccessible to wheeled or tracked vehicles, Boston Dynamics began developing BigDog, a four-legged machine roughly the size of a large Bernese mountain dog. The robot had to be able to navigate messy, unpredictable, real-world terrain. This meant it needed to be tough, exceptionally agile, able to carry its own power source, and  able to sense its own movement, and the environment, in greater detail than any of the walking machines built before.</p>

                                <p>“Most of the laboratory stuff we’d done up until BigDog was in a pretty benign lab environment,” Raibert says. “It was clean, it was dry, and it was flat.”</p>

                                <p>The resulting machine was powered by a go-kart engine, and it used 69 sensors to monitor the movement of its legs, the forces exerted on those limbs, and factors including temperature and hydraulic pressure. Using dynamic balance, it could walk across sand, snow, and even ice without toppling over. Most spectacularly, it could stay on its feet when given a strong kick. BigDog can be driven remotely, but like other Boston Dynamics robots, its balancing behavior is automatically controlled by an onboard computer.</p>
                            </div>

                        </div>

                        <img src="assets/imgs/ls3.jpg" alt="">

                        <div class="inner-wrap">

                            <div class="video left hide-for-mobile">
                                <div class="flex-video">
                                    <object id="myExperience3152882167001" class="BrightcoveExperience">
                                        <param name="playerID" value="3164827754001" />
                                        <param name="playerKey" value="AQ~~,AAAAAAEgZvo~,jStb8wH-jnK4A8jPSw5ObvdyN2i4vafT" />
                                        <param name="isVid" value="true" />
                                        <param name="isUI" value="true" />
                                        <param name="dynamicStreaming" value="true" />
                                        <param name="@videoPlayer" value="3152882167001" />
                                      </object>
                                </div>
                            </div>

                            <div class="text right">
                                <h2>LS3</h2>

                                <p>With additional military funding, including some money from the Marines, Boston Dynamics started building a larger, more powerful version of BigDog in 2009. Dubbed Alpha Dog, but officially called the Legged Squad Support System, or LS3, the robot is the size of a horse and can carry 180 kilograms, or four Marines’ fully loaded backpacks, for up to 20 miles a day over rough terrain.</p>

                                <p>Like BigDog, LS3 uses a laser ranging instrument, or LIDAR, and stereo video cameras in its head to identify obstacles, to map its surroundings, and to follow a soldier wearing a reflective patch walking up to 45 meters ahead. Last summer, the Marines began testing LS3 at a desert base in California and in the woods at Fort Devens in Massachusetts. These tests have involved simulated combat missions with LS3 as a pack mule.</p>
                            </div>
                        </div>

                        <img src="assets/imgs/wildcat.jpg" alt="">

                        <div class="inner-wrap">

                            <div class="video right hide-for-mobile">
                                <div class="flex-video">
                                    <object id="myExperience3152796575001" class="BrightcoveExperience">
                                        <param name="playerID" value="3164827754001" />
                                        <param name="playerKey" value="AQ~~,AAAAAAEgZvo~,jStb8wH-jnK4A8jPSw5ObvdyN2i4vafT" />
                                        <param name="isVid" value="true" />
                                        <param name="isUI" value="true" />
                                        <param name="dynamicStreaming" value="true" />
                                        <param name="@videoPlayer" value="3152796575001" />
                                    </object>
                                </div>
                            </div>

                            <div class="text left">
                                <h2>Wild Cat</h2>
                                <p>DARPA also provided funding for a more mobile, agile, and faster four-legged robot. The first version, Cheetah, can run at 47  kilometers per hour on a treadmill while attached to a stabilizing bar. Boston Dynamics developed a larger, untethered version, called WildCat, in 2013. Like Cheetah, WildCat flexes its body to extend its stride and increase its speed. It can run at 26 kilometers per hour via remote control. Boston Dynamics has posted video online of the robot bounding and galloping around its car park.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <!-- SECTION 3: Learning to Walk -->

            <section id="partThree" class="clearfix">
                <div class="title text-center">
                    <p>Part 3</p>
                    <h1>Learning to Walk</h1>
                </div>
                <div class="bg">
                    <div class="wrap">
                        <div class="text right">
                            <p>In 1989, one of Raibert’s graduate students, Rob Playter, who had been a champion gymnast at Ohio State, helped him build a free-roaming, <a href="http://www.ai.mit.edu/projects/leglab/robots/3D_biped/3D_biped.html" target="_blank">two-legged robot</a> that could perform somersaults and other acrobatic feats on a treadmill or while bounding around the lab. “The somersault was showing off,” Raibert admits. But it demonstrated a level of control that promised to help robots navigate much more difficult terrain. It also hinted at how machines might one day move through environments designed for humans. Wheels are a fine way to move when the ground ahead is flat and clear, but a wheel can’t easily climb stairs or get past an overturned chair. If robots are ever to be used widely in our homes, it is likely they will need to walk.</p>

                            <p>“Because we arrange our houses to suit human beings, it’s very important that the robots have the same competencies of locomotion and manipulation as human beings do,” says Gill Pratt, the DARPA program manager in charge of the robotics challenge. “Legs can provide a tremendous advantage over wheels and tracks; a leg doesn’t need a continuous path of support; a leg can step over things, which is an extraordinary thing to do.”</p>

                            <p>The specific inspiration for the DARPA Robotics Challenge came in dramatic circumstances, when an earthquake struck off the coast of Japan in March 2011. Attempts at cleaning up the damaged nuclear reactor at Fukishima highlighted the limitations of the best existing robots, and showed the need for machines that can better navigate the human world. DARPA devised its Robotics Challenge to inspire robots that could help should such a situation occur again. The robots must be able to not only work in environments designed for humans but also to navigate those sites after they are severely damaged.</p>

                            <p>Atlas performed well in Miami, but it is a long way from perfect. For one thing, the power needed to drive its hydraulic systems limits its usefulness. The robots deployed in the contest each required external generators to power their hydraulics; the generators are too large to carry, relatively inefficient, and loud. Even though future versions of Atlas are meant to carry their own power source, this will still be a rudimentary solution until researchers can figure how to make the machines far more energy efficient.</p>

                            <p>Perception is another big challenge. Atlas uses dynamic balance, and it can scan its surroundings for obstacles, but the way it uses this information to navigate is still slow and crude. “If you watch someone dancing or rock climbing or doing parkour, we are incredibly far [away from] a robot that can do that,” Pratt says.</p>

                            <p>During the DARPA challenge, Atlas operated partly autonomously, in that teams could command it to perform a task, and could provide specific instructions, but much of the robot’s behavior, including its split-second ability to rebalance itself, happened automatically. DARPA’s vision is for rescue robots to operate this way, with humans providing guidance and assistance, but the robots functioning autonomously when needed, such as when a communications link fails. But if robots are ever to perform the kinds of tasks that some envision—such as helping the elderly in the home—they will need to have the ability to work with even greater autonomy.</p>

                            <p>Back in the pit lane, near a garage commandeered by a support team from Boston Dynamics, Raibert says humans and animals have extraordinary mobility, more than any human-made vehicle, so it makes sense to make robots with legs. “Let me just say I think the future of robotics has got to go there,” Raibert says, just before one of his robots starts walking assuredly over a pile of rubble. “You can do stuff now without it, but eventually you’re really going to want that, and that’s why we’re hoping to enable.”</p>
                        </div>
                        <div id="atlas" class="robots left hide-for-mobile">
                            <div class="robot">
                                <div class="affix-wrap">
                                    <img src="assets/imgs/atlas1.jpg" alt="">
                                </div>
                                <small>Atlas is designed to perform tasks in human environments.</small>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="atlas-flip-book">
                    <div class="page-1"></div>
                    <div class="page-2"></div>
                    <div class="page-3"></div>
                </div>

            </section>
        
        </article>
        
        <footer>
            <div class="wrap">
                <p class="return-to-tech-review"><a href="">Home</a></p>
                <p class="scroll-to-top"><a href="#" onclick="$('html, body').animate({ scrollTop: '0px' });event.preventDefault();">Back to top</a></p>
            </div>
        </footer>

        <!--
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.js"></script>
    -->
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.10.1.js"><\/script>')</script>


        <script src="assets/js/vendor/brightcove.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/vendor/jquery.backgroundvideo.min.js"></script>
        <!--<script src="assets/js/vendor/jquery.scrolldepth.min.js"></script>-->
        <script src="assets/js/vendor/waypoints.min.js"></script>
        <script src="assets/js/vendor/jquery.smoothscroll.min.js"></script>
        <script src="assets/js/main.js"></script>

    

        <!--
        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    -->
    </body>
</html>
